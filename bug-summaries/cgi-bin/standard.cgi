#! /usr/bin/perl -Tw

require '/srv/bugs.qa.debian.org/perl/bug-summary.pl';

use CGI qw(:standard);

my $basepkgs = '/srv/bugs.qa.debian.org/data/lists/basedebs';
my $standardpkgs = '/srv/bugs.qa.debian.org/data/lists/standard';
my %basepkgs = map { binary_to_source($_) => 1 } read_package_list ($basepkgs);
my %allstandardpkgs = map { binary_to_source($_) => 1 }
			  read_package_list ($standardpkgs); # uniquifies too
my @standardpkgs;

for my $package (sort keys %allstandardpkgs) {
    unless ($basepkgs{$package}) {
	push @standardpkgs, $package;
    }
}

html_header ('Bugs in the standard installation');
show_table (@standardpkgs);
html_footer ();
