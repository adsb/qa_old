#!/usr/bin/env python

import os, subprocess, sys, urlparse

def error(msg):
	print 'Status: 500 Internal error'
	print 'Content-Type: text/plain'
	print
	print msg
	exit(1)

basedir = os.path.abspath(os.path.join(os.path.dirname(os.path.abspath(__file__)),'..'))
env = os.environ
url = env.get('REDIRECT_URL') or env.get('SCRIPT_URL') or error('Web server did not pass a URL to the script')
root, ext = os.path.splitext(url)

if ext != '.png':
    error('Only handle requests for PNG images')

h = 300
w = 730
m = 1

if 'QUERY_STRING' in os.environ:
    args = urlparse.parse_qs(os.environ['QUERY_STRING'])
    if 'h' in args:
        h = args['h'][0]
    if 'w' in args:
        w = args['w'][0]
    if 'm' in args:
        m = args['m'][0]

# root looks like /data/bts/graphs/n/name, /data/bts/graphs/multi/name0,name1
# or /data/bts/graphs/all
root = root.split('/')
# We only accept 
if len(root) < 5 or len(root) > 6 or (len(root) == 5 and root[4] != "all"):
    error('URL does not fit the scheme')

if root[4] == "by-maint":
    import bsddb
    db = bsddb.btopen(os.path.join(basedir, 'data', 'ddpo', 'results', 'archive.db'), 'r')
    maint = root[5]
    if maint.find('@') == -1:
        maint += '@debian.org'
    packages = []
    for c in ['main', 'contrib', 'non-free']:
        if '%s:%s' % (maint, c) in db:
            packages += [ p.rstrip('#') for p in db['%s:%s' % (maint, c)].split()]
    db.close()
else:
    packages = root[-1].split(',')
    # Only accept multiple package names under /bts/graphs/multi
    if root[4] != "multi" and len(packages) > 1:
        error('Only accept multiple package names under /bts/graphs/multi')

# root[:3] should be '/data/bts/graphs'.split()
basedir = reduce(lambda x, y: os.path.join(x, y), [basedir] + root[:4])

defs = []
extra_defs = []
count = 0

for package in packages:
    if package == "all":
        rrd = os.path.join(basedir, 'all.rrd')
    else:
        rrd = os.path.join(basedir, package[:4] if package.startswith("lib") else package[:1], package + '.rrd')
    if os.path.exists(rrd):
        defs += [ 'DEF:_rc%d=%s:RC:LAST' % (count, rrd),
                  'DEF:_in%d=%s:IN:LAST' % (count, rrd),
                  'DEF:_mw%d=%s:MW:LAST' % (count, rrd),
                  'DEF:_fp%d=%s:FP:LAST' % (count, rrd) ]
        if os.path.exists(rrd[:-3] + 'graph'):
            with open(rrd[:-3] + 'graph', 'r') as f:
                extra_defs += [l.strip() for l in f.readlines()]
        count += 1

if len(defs):
    for d in [ 'rc', 'in', 'mw', 'fp' ]:
        vars = [ '%s%d' % (d, i) for i in range(0, count) ]
        # The TRENDNAN thing avoids holes in the current data
        defs += [ 'CDEF:%(v)s=_%(v)s,UN,_%(v)s,3600,TRENDNAN,_%(v)s,IF' % { 'v': v } for v in vars ]
        defs += [ 'CDEF:%s=%s' % (d, ','.join(vars + ['ADDNAN'] * (count - 1))) ]
        defs += [ 'CDEF:_%s=%s' % (d, ','.join(['_' + v for v in vars] + ['ADDNAN'] * (count - 1))) ]
    defs += [ 'CDEF:total=rc,in,+,mw,+,fp,+',
              'CDEF:_total=_rc,_in,+,_mw,+,_fp,+',
              'AREA:rc#FF0000:RC\:',
              'GPRINT:_rc:LAST:%5.0lf',
              'AREA:in#EA8F00:I & N\::STACK',
              'GPRINT:_in:LAST:%5.0lf',
              'AREA:mw#EACC00:M & W\::STACK',
              'GPRINT:_mw:LAST:%5.0lf',
              'AREA:fp#EAFF00:F & P\::STACK',
              'GPRINT:_fp:LAST:%5.0lf',
              'LINE1:total#000000:Total\:',
              'GPRINT:_total:LAST:%5.0lf\\j' ]
else:
    defs = [ 'AREA:0#FF0000:RC\:                  0',
             'AREA:0#EA8F00:I & N\:                  0:STACK',
             'AREA:0#EACC00:M & W\:                  0:STACK',
             'AREA:0#EAFF00:F & P\:                  0:STACK',
             'LINE1:0#000000:Total\:                  0' ]

if 'GATEWAY_INTERFACE' in os.environ:
    print 'Content-Type: image/png\n'
    sys.stdout.flush()

subprocess.call(
    ['rrdtool', 'graph', '-h', str(h), '-w', str(w), '-l', '0', '-m', str(m),
     '--start=-31536000',
     '--title=Bug count for %s' % (packages[0] if len(packages) == 1 else 'multiple packages'),
     '--slope-mode',
     '--grid-dash', '1:0',
     '--alt-y-grid',
     '--right-axis', '1:0',
     '--right-axis-format', '%.0lf',
     '-' ] + defs + extra_defs,
    env = { 'RRD_DEFAULT_FONT': 'Sans', 'TZ': 'UTC' })
