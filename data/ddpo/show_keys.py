#!/usr/bin/env python2.1
#  Copyright (C) 2004 Igor Genibel                                         
#                                                                          
#  This program is free software; you can redistribute it and/or           
#  modify it under the terms of the GNU General Public License             
#  as published by the Free Software Foundation; either version 2          
#  of the License, or (at your option) any later version.                  
#                                                                          
#  This program is distributed in the hope that it will be useful,         
#  but WITHOUT ANY WARRANTY; without even the implied warranty of          
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           
#  GNU General Public License for more details.                            
#
#  This code is currently maintained and debugged by Igor Genibel, any     
#  questions or comments regarding this code should be directed to:        
#  - igor@genibel.org                                                      

# This script is used by ddpo in order to provide a gpg keys cache
# stored into a dbm file. It seriously improves the backend generation

#  - Igor Genibel - http://genibel.org/                                    


import dbm, re, os, fileinput

prefix = "/srv/qa.debian.org/data/ddpo/results"

keys = dbm.open(prefix + "/keys", 'r')
for key in keys.keys():
    try:
     print 'email: ' + key + " has gpg key: " + keys[key]
    except:
     pass
keys.close()
