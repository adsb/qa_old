BEGIN;

SET search_path TO public;

CREATE LANGUAGE plpgsql;

/* built-in in PG 8.4:
CREATE AGGREGATE array_agg (anyelement)
(
	sfunc = array_append,
	stype = anyarray,
	initcond = '{}'
);
*/

COMMIT;
