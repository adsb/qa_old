MIA Tracking Database
=====================

This is a collection of python scripts to keep track of maintainers who are
Missing In Action (MIA).  If you think a maintainer is MIA, read on.


Verifying MIAness
-----------------

First, you should check if the database already knows about the maintainer:
  $ ./mia-query myon@debian.org
(Sorry, this tool is only available to developers.)

The DDPO pages are a nice overview to see NMUs and bug counts:
  https://qa.debian.org/developer.php?login=myon@debian.org&ordering=1
Ordering by last upload date makes spotting the oldest packages easier.

You should then check the PTS for uploads, and the BTS for maintainer followups
on open bugs.  (This is not automated yet, unfortunately.)

The echelon information from LDAP shows the last activity on Debian lists
identified by "From:" headers and gpg signatures.
  $ ldapsearch -u -x -H ldap://db.debian.org -b dc=debian,dc=org uid=myon |
    grep -A2 activity

Usually, there should not have been any substantial activity for at least 3,
most often 6 months, before pestering the maintainer is warranted, because
people often drop out for shorter periods and come back themselves.  Also, some
developers are very active for Debian doing various things but neglecting their
packages.  They should not be added either, except if the packages look very
bad.


Notifying the MIA team
----------------------

The easiest way is to mail <mia@qa.debian.org>.  The people reading this
address will then take care of further steps, i.e. they will put the message in
the database, investigate the situation, ping the maintainer, and depending on
the outcome, orphan the packages, or suggest other action to undertake.

Please note that due to the amount and diverse nature of messages sent to
<mia@qa.debian.org>, we usually do not acknowledge receiving your input.  If
you want to have feedback, use the 'mia-query' tool, ask on #debian-qa on
irc.oftc.net, or state so in your message.

[Stop reading here if you just wanted to report a single maintainer.]


Adding information to the database
----------------------------------

You can add information directly to the database by sending e-mail to
mia-<id>@qa.debian.org.  'id' is either the LDAP login name, or the email
address with @ replaced by '='.  (Please don't expose these addresses in the
BTS/on the web/etc., so using 'Bcc' is encouraged.)

Every e-mail added to the database needs a summary.  If your e-mail has an
X-MIA-Summary header, then the header will be used for the summary.  Otherwise,
an e-mail is sent to mia@qa.debian.org asking for a summary.  The 'mia-bounce'
script is handy to bounce messages to the database (requires 'formail').

If unsure, send your message to mia@qa.debian.org instead and we will put the
message into the database for you.


Encoding in the database
------------------------

The MIA database consists of a mbox per maintainer in the db/ subdirectory.
The corresponding status is stored in .summary files alongside.  Via the
X-MIA-Summary field and via a response to the mail if such mail was not
present, one can set one of the above status items. 'none' is not settable,
all the others are simply settable by mentioning them. You can set multiple such
entries by separating them with a comma, a semi-colon will terminate the
parseable section, you can add a short summary of the contents of the mail
after the ';'.

'mia-query' allows you to query the database. Run mia-query --help to get help
about the usage.

Short summary for the other tools;
* mia-todo: prints a todo list of maintainers where action is required.
  Use --help to see the various levels to query information.
* mia-save: allows to append summary lines to the database that are not related
  to incoming messages. (This is only possible if you're in the 'qa' unix group.)
* mia-bounce: helper script to inject X-MIA-Summary headers and bounce messages
  to the MIA database. (Use it at home, e.g. calling it from mutt directly)
* mia-record: the backend tool that processes messages sent to
  mia-*@qa.debian.org.


Available summary tags
----------------------

There are two summary axes: status and prod level:

- Status: How badly MIA does this maintainer look/is this maintainer?
  - none         # not listed

  The following tags are incremental, they don't decrease except after an 'ok',
  which is a reset.
  - ok           # without parameter: reset status
  - busy         # Some activity, but looks time-starved
  - inactive     # no activity at all for a while (> 3m)
  - unresponsive # no response from MIA pings, or response but no action
  - retiring     # maintainer noted intent to retire (or drop out of NM), but
                   did not follow up yet
  - mia          # No response even to last ping, all packages can be orphaned at
                   sight, wat can be sent
  - needs-wat    # No packages anymore, but has Debian account ("where art thou")
  - retired      # Has officially announced retirement (or termination of
                   packaging involvement for non-DDs)
  - removed      # Account was removed in a WAT run (or all packaging involvement
                   terminated by the MIA team for non-DDs)
  - role         # Is not a flesh and blood maintainer at all, but mailinglist or
                   other role address

  The above base status tags can be temporarily suspended by the following:
  - ok for <time> # To override a non-ok severity temporarily, for example, if
                    the overall situation doesn't seem to warrant a(nother)
                    prod, this enables you to postpone. <time> is 2m3w2d" for
                    example, and means that the validity of 'ok' is valid for
                    2 months, 3 weeks and 2 days)
  - npa [for <t>] # similar to 'ok', means 'non-packaging activity'.  Also
                    protects against wat in case of no packages left.  Examples
                    are buildd admins, porters, AMs.  Defaults to 1y review.
  - willfix [for <t>]       # similar to 'ok', means promise of resuming/increasing
                    activity. Suspends more severe status until expiring
                    (default 4w)
  If you want to preliminary terminate a too-long suspension, use '<suspend>
  for 0d'.

- Prod-level:
  - none         # No pings been sent since last 'ok'
  - nice         # Asked nicely whether someone maybe is lacking sufficient time/interest
  - prod         # Noted some packages look bad, and asked what's up and whether
                   maybe some/all packages are better orphaned
  - last-warning # Noted there is no improvement at all, warned that we'll proceed
                   orphaning
  - wat          # warning about account disabling has been sent

  You can also indicate a mail from MIA to the maintainer, or a response from
  the maintainer to MIA, by using 'out' and 'in' respectively. Use neither
  'out' nor 'in' for all other mails (external hints, observed mails/activity
  from maintainer).

  Prod-levels also act like a suspend to allow the maintainer to reply,
  similarly they take ' for 1w' as parameter (defaults to 3 weeks).

Mailing the maintainer
----------------------

First of all, be nice to people.  We are pestering them, this is already rude
enough.  Ask them how they are doing.  Including a "thanks for your
contribution to Debian" is always a nice thing (even if they have been MIA for
years), etc...

It is tempting to have templates for the messages you send.  However,
hand-writing the messages makes them much more personal.  The reasons people
are MIA are so diverse that a template is usually only good for the very first
contact.

Finally, while answering to replies maintainers send is not always required, it
is a nice gesture.  Don't send MIA-pings if you know you will be away for some
days.

After 3 weeks, you should contact the maintainer again if they did not update
their packages in the meantime.  The 'mia-todo' tool tells you which
maintainers need being contacted.


Orphaning a package
-------------------

If a maintainer has been contacted several times and the package has still
not been fixed, it is time to orphan the package.  The script wnpp-orphan
can be used to generate a template to file a proper WNPP bug.  It will
generate a nice message and include some information about the package.
wnpp-orphan takes one or more packages as its arguments.  Furthermore, you
can specify an action with -a.  The action determines which template is
used to generate the WNPP bug report.  The default action is "mia".  It
asks for a justification why the package is being orphaned (this can be
left blank, though) and then generates a template which will CC the
maintainer and also Bcc the MIA database.  This action is the most useful
for doing MIA work.  However, the other actions might come in handy too.
They are:

  - qa: file a proper WNPP bug for a package that has been orphaned by
    the maintainer and is now owned by QA.  This action is useful in
    combination with wnpp-publicize (in /srv/qa.debian.org/data/wnpp).
  - orphan: to orphan a package
  - rfa: to put a package up for adoption

The mutt-orphan script can also be used to orphan a maintainer packages
immediately. These orphan bugs are also appended to maintainer's record
in MIA database (Bcc).

For co-maintained packages the remove-uploader script should be used instead.
It files a minor bug on that package, asking for removal of the MIA uploader.
Bugs are also usertagged, and so they can easily seen and tracked in
https://bugs.debian.org/cgi-bin/pkgreport.cgi?users=mia@qa.debian.org;tag=mia-teammaint

The mutt-disclaim script can be used for open WNPP bugs the maintainer
could have opened, in order to bring them back to RFP status.

These three scripts use mutt MUA for sending messages, so you probably
want mutt installed and properly configured before using them.

Finding inactive maintainers
----------------------------

There is currently no specific tool to find MIA maintainers and we are unsure
if we even want to automate this task.  One way to systematically find MIA
people is subscribing to debian-devel-changes and looking for NMUs.  However,
note that an NMU doesn't necessarily imply that a particular maintainer is
inactive.  There must be a series of NMUs over some time period, otherwise
you cannot say someone is really inactive.  Looking at the changelogs of
all the packages of a maintainers gives a pretty good picture of the
activity of the maintainer.  Many NMUs happen when a FTBFS bug is filed and
the maintainer doesn't upload a fix in a reasonable time.  Hence, it makes
sense to look for bugs which are tagged "fixed" and which are submitted by
buildd maintainers.  One buildd maintainer's address is james@nocrew.org, so
you can see all the bugs he filed and which are fixed now when you go to
https://bugs.debian.org/cgi-bin/pkgreport.cgi?which=submitter&data=james@nocrew.org&archive=no&include=fixed

Other sources are lintian warnings, and mass bug filings like library
transitions or policy updates (like /usr/doc -> /usr/share/doc).


Sponsored people who are inactive
---------------------------------

A major problem are sponsored people who are inactive.  They are even
harder to pin down than inactive developers since no echelon information
is available about them, their contact details are not available on
db.debian.org, etc.  One strategy which might be useful to find out about
their current status is to contact the developer who has actually uploaded
the package.  After all, they are responsible for the upload and should
know what happened to the person they sponsored.  Furthermore, one can
check https://nm.debian.org and see the status of the person in the NM
process.  The site also lists the Application Manager of the applicant and
they might have more information.

One problem is that there is no list of sponsored people.  Colin Watson
used the following scripts as a starting point to find sponsored people:

  grep-available -rFMaintainer -nsMaintainer . > maintainers

  (gpg --no-default-keyring --keyring ./debian-keyring.gpg --list-keys; \
   gpg --no-default-keyring --keyring ./debian-keyring.pgp --list-keys) \
    | grep -v '^\[.*\]$' | grep . | sort -u > keyring-maintainers

  perl -e 'open K, "keyring-maintainers";
           while (<K>) { $k{lc $_} = 1; }
           close K;
           open M, "maintainers";
           while (<M>) { print unless $k{lc $_}; }
           close M;' > sponsored-maintainers

However, this is only an approximation.  It's still necessary to go through
the file by hand and remove the ones who use different e-mail addresses from
the ones in their GPG keys.  Thus, most of the work is weeding through the
sponsored-maintainers file and comparing it more intelligently to
keyring-maintainers.  Once a current listing of sponsored people has been
made with this method, one can start checking the status of all sponsored
people against https://nm.debian.org/ and see who's left, or which applicants
who are "on hold" in the NM process have packages in the archive.

[Update: this section is outdated, carnivore improves the situation considerably]


Co-maintainers
--------------

Some people are only co-maintainers (Uploaders: in the control file) of
packages.  Depending on how the maintenance team works, it can be hard to track
if a certain person has done any work.  Additionally, there is currently no
standard procedure to request an inactive co-maintainer to be removed from the
uploaders list, resulting in many obsolete entries in the list of all
maintainers.  Maintainers should be encouraged to drop co-maintainers from
their packages if they do not contribute.  If you take over a package, please
remove the original maintainer unless they are going to contribute in the
future.


Some final thoughts
-------------------

What do we expect from maintainers?  Do we want volunteers who are there
all the time or do we accept that volunteers are busy with other stuff
sometime?  Is an NMU a normal and accepted thing (as it used to be a couple
of years ago) or is it a bad thing to get an NMU (as it is nowadays)?
Should one person maintain a package or is it ok if other people help out
sometimes?

The MIA effort should increase the quality in Debian, but it must not
become a witch hunt.


Getting involved
----------------

All new additions made to the MIA DB are sent to the mia@qa.debian.org
alias.  If you want to be involved with finding MIA maintainers, ask
mia@qa.debian.org to subscribe you.  You do not even have to be a Debian
Developer, there are ways to get you access to the data and tools you need.

If you have any questions or good ideas, please email mia@qa.debian.org.

 -- Jeroen van Wolffelaar <jeroen@debian.org>
    Christoph Berg <myon@debian.org>  Thu, 30 Mar 2006 15:10:35 +0200

# vim: et
