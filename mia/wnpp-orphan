#!/usr/bin/env python

# Generate templates to orphan packages
# Copyright (C) 2002, 2003, 2004, 2005  Martin Michlmayr <tbm@cyrius.com>

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

import re, textwrap, string, sys, os
import apt_pkg, status, utils

info = {}
maintainer = {}
uid = {}
uploaders = {}
description = {}
justification = None


def help():
    print """Usage: wnpp-orphan [OPTION]... <PACKAGE | EMAIL ADDRESS>[...]
Generate templates to orphan packages

Options:
  -a, --action              Action (%s)
""" % string.join(Cnf.value_list("WNPP-Orphan::Actions"), ", ")
    sys.exit(1)


def parse_args():
    global Cnf, Options

    apt_pkg.init()
    Cnf = apt_pkg.Configuration()
    apt_pkg.read_config_file_isc(Cnf, status.config)

    Arguments = [("h", "help", "WNPP-Orphan::Options::Help"),
                 ("a", "action", "WNPP-Orphan::Options::Action", "HasArg")]
    for i in ["Help", "Action"]:
        if not Cnf.exists("WNPP-Orphan::Options::%s" % i):
            Cnf["WNPP-Orphan::Options::%s" % i] = ""

    to_orphan = apt_pkg.parse_commandline(Cnf, Arguments, sys.argv)
    if not to_orphan:
        print "E: You have to specify at least one package."
        help()

    Options = Cnf.subtree("WNPP-Orphan::Options")

    if Options["Help"]:
        help()

    # Use the first action from the config file as the default action
    default_action = Cnf.value_list("WNPP-Orphan::Actions")[0]
    if not Options["Action"]:
        Options["Action"] = default_action
    elif Options["Action"] not in Cnf.value_list("WNPP-Orphan::Actions"):
        print "W: Unknown action option '%s', using default '%s'." % (Options["Action"],
            default_action)
        Options["Action"] = default_action

    return to_orphan

def get_orphan_info(to_orphan):
    re_maint_email = re.compile(r"<(.*)>")
    orphan = []
    matched = []

    try:
        sources = os.popen(Cnf["WNPP-Orphan::SourcesCommand"])
        packages = os.popen(Cnf["WNPP-Orphan::PackagesCommand"])
    except (IOError, OSError):
        print "Sources or packages file cannot be opened!"
        sys.exit(1)

    sources_parser = apt_pkg.TagFile(sources)
    while sources_parser.Step():
        package = sources_parser.Section.get("Package")
        binaries = [string.strip(binary) for binary in sources_parser.Section.get("Binary").split(",")]
        result = re_maint_email.search(sources_parser.Section.get("Maintainer"))
        if result:
            maint_mail = result.group(1)
        for to_find in to_orphan:
            if to_find in binaries + [package, maint_mail]:
                matched.append(to_find)
                if to_find == maint_mail:
                    print "I: %s maintains %s" % (to_find, package)
                    to_find = package
                elif to_find != package:
                    # A binary instead of a source package was given
                    print "W: mapping binary package %s to source package %s" % (to_find, package)
                    to_find = package
                # This source package has not been processed yet
                if to_find not in orphan:
                    orphan.append(to_find)
                    # initialize description[to_find]: this is necessary because there
                    # would be an error if the package cannot be found in the packages
                    # file (e.g. a powerpc specific package in an i386 packages file).
                    description[to_find] = "no description"
                    # Generate some printable information about the source package
                    info[to_find] = ""
                    for tag in sources_parser.Section.keys():
                        info[to_find] += tag + ": " + sources_parser.Section.get(tag) + "\n"
                    info[to_find] += "\n"
                    # Get some important information needed to generate the template
                    maintainer[to_find] = sources_parser.Section.get("Maintainer")
                    if sources_parser.Section.get("Uploaders"):
                        uploaders[to_find] = sources_parser.Section.get("Uploaders")
                    # Try extracting the Debian UID
                    domain = "@" + Cnf["MyDomain"]
                    if maint_mail.endswith(domain):
                        uid[to_find] = string.replace(maint_mail, domain, "")
    # all tried
    for todo in to_orphan[:]:
        if todo in matched:
            to_orphan.remove(todo)

    if to_orphan:
        print "E: Cannot find %s." % string.join(to_orphan, ", ")

    packages_parser = apt_pkg.TagFile(packages)
    while packages_parser.Step():
        package = packages_parser.Section.get("Package")
        source = packages_parser.Section.get("Source", package)
        if source in orphan:
            # Generate some printable information about each binary package
            for tag in packages_parser.Section.keys():
                info[source] += tag + ": " + packages_parser.Section.get(tag) + "\n"
            info[source] += "\n"
            description[source] = string.split(packages_parser.Section.get("Description"), "\n")[0]

    return orphan

def gen_template(source, counter):
    global justification
    Subst = {}
    Subst["__MAINTAINER__"] = maintainer[source]
    Subst["__SOURCE__"] = source
    Subst["__DESCRIPTION__"] = description[source]
    Subst["__PACKAGE_INFORMATION__"] = string.rstrip(info[source])

    print "W: Orphaning %s..." % source

    if uploaders.has_key(source):
        print "Package has the following uploaders:"
        print uploaders[source]
        sys.stdout.write("Continue (y/N)? ")
        sys.stdout.flush()
        confirm = raw_input().lower()
        if confirm != "y":
            return 0

    if Options["Action"] == "mia":
        if not uid.has_key(source):
            sys.stdout.write("Login name of developer: ")
            sys.stdout.flush()
            uid[source] = raw_input()
        Subst["__UID__"] = uid[source]
        # Ask for a justification if we never asked for one before
        if justification == None:
            sys.stdout.write("Justification: ")
            sys.stdout.flush()
            justification = raw_input()
        if justification:
            Subst["__JUSTIFICATION__"] = "\n" + string.join(
                textwrap.wrap("Justification: " + justification, 76), "\n")
        else:
            Subst["__JUSTIFICATION__"] = ""

    template = utils.TemplateSubst(Subst, Cnf["Dir::Templates"]+"/wnpp-orphan.%s" % Options["Action"])
    filename = "%03d-%s.txt" % (counter, source)
    try:
        output = open(filename, "w")
    except (IOError, OSError):
        print "E: Cannot write output file '%s'." % filename
    print "W: Writing template to %s." % filename
    output.write(template)
    output.close()
    return 1


# main

to_orphan = [string.replace(x, ",", "") for x in parse_args()]
orphan = get_orphan_info(to_orphan)
counter = 1
for source in orphan:
    counter += gen_template(source, counter)

# vim: ts=4:expandtab:shiftwidth=4:
