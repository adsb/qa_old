CREATE TABLE popcon_day (
	day date PRIMARY KEY,
	submissions integer NOT NULL
);

CREATE TABLE popcon_architecture (
	architecture text,
	day date REFERENCES popcon_day (day),
	submissions integer NOT NULL,
	PRIMARY KEY (architecture, day)
);

CREATE TABLE popcon_release (
	release text,
	day date REFERENCES popcon_day (day),
	submissions integer NOT NULL,
	PRIMARY KEY (release, day)
);

CREATE TABLE popcon_vendor (
	vendor text,
	day date REFERENCES popcon_day (day),
	submissions integer NOT NULL,
	PRIMARY KEY (vendor, day)
);

-- package names are often long, create ids for them so the popcon table is smaller
CREATE TABLE popcon_package (
	id SERIAL PRIMARY KEY,
	package text NOT NULL,
	in_debian boolean NOT NULL DEFAULT false,
	-- most recent submission data:
	day date,
	vote integer,
	old integer,
	recent integer,
	no_files integer
);
CREATE UNIQUE INDEX ON popcon_package (package);

CREATE TABLE popcon (
	package_id integer,
	day date,
	vote integer NOT NULL,
	old integer NOT NULL,
	recent integer NOT NULL,
	no_files integer NOT NULL,
	PRIMARY KEY (package_id, day)
);

CREATE VIEW popcon_v AS
	SELECT package, p.day, vote, old, recent, no_files, submissions, in_debian
	FROM popcon_package pp join popcon p ON (pp.id = p.package_id)
	JOIN popcon_day pd ON (pd.day = p.day);

GRANT SELECT ON popcon_day, popcon_architecture, popcon_release, popcon_vendor, popcon_package, popcon TO PUBLIC;

-- now import the data ...

ALTER TABLE popcon ADD PRIMARY KEY (package_id, day),
	ADD FOREIGN KEY (package_id) REFERENCES popcon_package (id),
	ADD FOREIGN KEY (day) REFERENCES popcon_day (day);

CLUSTER popcon_architecture USING popcon_architecture_pkey;
CLUSTER popcon_release USING popcon_release_pkey;
CLUSTER popcon_vendor USING popcon_vendor_pkey;
CLUSTER popcon USING popcon_pkey;
